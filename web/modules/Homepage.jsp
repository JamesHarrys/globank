<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- ====== Hero Area ====== -->
<div class="hero-aria" id="home">
    <!-- Hero Area Content -->
    <div class="container">
        <div class="hero-content h-100">
            <div class="d-table">
                <div class="d-table-cell">
                    <h2 class="text-uppercase">Let's Begin</h2>
                    <h3 class="text-uppercase"><span class="typed"></span></h3>
                    <p>A marketing solution by Globank.</p>
                    <a href="#about" class="button smooth-scroll">Learn More</a>
                </div>
            </div>
        </div>
    </div>
    <!-- // Hero Area Content -->
    <!-- Hero Area Slider -->
    <div class="hero-area-slids owl-carousel">
        <div class="single-slider">
            <!-- Single Background -->
            <div class="slider-bg" style="background-image: url(assets/images/hero-area/img-1.jpg)"></div>
            <!-- // Single Background -->
        </div>
        <div class="single-slider">
            <!-- Single Background -->
            <div class="slider-bg" style="background-image: url(assets/images/hero-area/img-2.jpg)"></div>
            <!-- // Single Background -->
        </div>
    </div>
    <!-- // Hero Area Slider -->
</div>
<!-- ====== //Hero Area ====== -->

<!-- ====== Featured Area ====== -->
<section id="featured" class="section-padding pb-70">
    <div class="container">
        <div class="row">
            <!-- single featured item -->
            <div class="col-lg-4 col-md-6">
                <div class="single-featured-item-wrap">
                    <h3><a href="#">Customer Base</a></h3>
                    <div class="single-featured-item">
                        <div class="featured-icon">
                            <i class="fa fa-edit"></i>
                        </div>
                        <p>Our bank is for everyone whether you are a young entrepreneur, a student or a salesman.</p>
                    </div>
                </div>
            </div>
            <!-- single featured item -->
            <!-- single featured item -->
            <div class="col-lg-4 col-md-6">
                <div class="single-featured-item-wrap">
                    <h3><a href="#">Targeted Advertising</a></h3>
                    <div class="single-featured-item">
                        <div class="featured-icon">
                            <i class="fa fa-code"></i>
                        </div>
                        <p>Our bank is at your service 24/24, a question a advised will be the one for you in the shortest time.</p>
                    </div>
                </div>
            </div>
            <!-- single featured item -->
            <!-- single featured item -->
            <div class="col-lg-4 col-md-6">
                <div class="single-featured-item-wrap">
                    <h3><a href="#">Business Services</a></h3>
                    <div class="single-featured-item">
                        <div class="featured-icon">
                            <i class="fa fa-search"></i>
                        </div>
                        <p>Our bank offers you diferente possibility going from the near to study to the mortgage credit all while passing by bank booklet as well as insurance.</p>
                    </div>
                </div>
            </div>
            <!-- single featured item -->
        </div>
    </div>
</section>
<!-- ====== //Featured Area ====== -->

<!-- ====== About Area ====== -->
<section id="about" class="section-padding about-area bg-light">
    <div class="container">
        <!-- Section Title -->
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <div class="section-title text-center">
                    <h2>About Us</h2>
                    <p>To be able to count on your bank 24/24, to be free to change your mind, to have confidence, to see your loyalty recognized ...</p>
                </div>
            </div>
        </div>
        <!-- //Section Title -->
        <div class="row">
            <div class="col-lg-6">
                <div class="about-bg" style="background-image:url(assets/images/about-bg.jpg)">
                    <!-- Social Link -->
                    <div class="social-aria">
                        <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                        <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fab fa-youtube"></i></a>
                    </div>
                    <!-- // Social Link -->
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-content">
                    <h2>Hello, we are <span>Globank</span></h2>
                    <h4>Bank & Marketing Solutions</h4>
                    <p>After several months of research and development our team is proud to present globank the right solution for your needs. Need a credis? Thanks to a revolutionary algoritm you will have the information in a few seconds. Need to open a booklet, made in a few clicks. Globank the right solution for your needs.</p>


                </div>
            </div>
        </div>
    </div>
</section>
<!-- ====== // About Area ====== -->


<!-- ====== Fact Counter Section ====== -->

<section class="section-padding pb-70 bg-img fact-counter" id="counter" style="background-image: url(assets/images/fan-fact-bg.png)">
    <div class="container">
        <div class="row">
            <!-- Single Fact Counter -->
            <div class="col-lg-3 co col-md-6 l-md-6 text-center">
                <div class="single-fun-fact">
                    <h2><span class="counter-value" data-count="03">0</span>+</h2>
                    <p>Years Experience</p>
                </div>
            </div>
            <!-- // Single Fact Counter -->
            <!-- Single Fact Counter -->
            <div class="col-lg-3 col-md-6 text-center">
                <div class="single-fun-fact">
                    <h2><span class="counter-value" data-count="85">0</span>+</h2>
                    <p>Happy Clients</p>
                </div>
            </div>
            <!-- // Single Fact Counter -->
            <!-- Single Fact Counter -->
            <div class="col-lg-3 col-md-6 text-center">
                <div class="single-fun-fact">
                    <h2><span class="counter-value" data-count="03">0</span>+</h2>
                    <p>Awards Won</p>
                </div>
            </div>
            <!-- // Single Fact Counter -->
            <!-- Single Fact Counter -->
            <div class="col-lg-3 col-md-6 text-center">
                <div class="single-fun-fact">
                    <h2><span class="counter-value" data-count="851">0</span>+</h2>
                    <p>Cups of Coffee</p>
                </div>
            </div>
            <!-- // Single Fact Counter -->
        </div>
    </div>
</section>
<!-- ====== //Fact Counter Section ====== -->


<!-- ====== Testimonial Section ====== -->
<section id="testimonial" class="section-padding bg-secondary testimonial-area">
    <div class="container bg-white">
        <!-- Section Title -->
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <div class="section-title text-center">
                    <h2>Testimonial</h2>
                    <p>Some testimonials from our customers.</p>
                </div>
            </div>
        </div>
        <!-- //Section Title -->
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="testimonials owl-carousel" data-ride="carousel">
                    <!-- Single Testimonial -->
                    <div class="single-testimonial text-center">
                        <div class="testimonial-quote">
                            <i class="fa fa-quote-right"></i>
                        </div>
                        <p>Since I'm registered on globank no more account management concerns everything is automated, and I can have the information I want in real time.</p>
                        <h4>Aseven M <span>CEO - aseven.info</span></h4>

                    </div>
                    <!-- // Single Testimonial -->
                    <!-- Single Testimonial -->
                    <div class="single-testimonial text-center">
                        <div class="testimonial-quote">
                            <i class="fa fa-quote-right"></i>
                        </div>
                        <p>Globank has changed my life, no worries banker globank is there.</p>
                        <h4>Laurie P <span>CEO - Laurie&co.info</span></h4>

                    </div>
                    <!-- // Single Testimonial -->
                    <!-- Single Testimonial -->
                    <div class="single-testimonial text-center">
                        <div class="testimonial-quote">
                            <i class="fa fa-quote-right"></i>
                        </div>
                        <p>Need a credited or a request for insurance thanks to its revolutionary system globank answers me instantly.</p>
                        <h4>Morgan F <span>CTO - SilverCompany.info</span></h4>

                    </div>
                    <!-- // Single Testimonial -->
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ====== // Testimonial Section ====== -->

<!-- ====== Team Section ====== -->
<section class="section-padding pb-70 team-area">
    <div class="container">
        <!-- Section Title -->
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <div class="section-title text-center">
                    <h2>Team</h2>
                    <p>Our development team.</p>
                </div>
            </div>
        </div>
        <!-- //Section Title -->
        <div class="row">
            <!-- Single Team -->
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-team">
                    <div class="team-thumb" style="background-image: url(assets/images/team/JM.jpg)">
                        <div class="team-social">
                            <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-pinterest"></i></a>
                        </div>
                    </div>
                    <div class="team-content">
                        <h4>Jean-Marc BISSICK</h4>
                        <span>CEO & Founder</span>
                    </div>
                </div>
            </div>
            <!-- // Single Team -->
            <!-- Single Team -->
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-team">
                    <div class="team-thumb" style="background-image: url(assets/images/team/axel.jpg)">
                        <div class="team-social">
                            <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-pinterest"></i></a>
                        </div>
                    </div>
                    <div class="team-content">
                        <h4>Axel Aubry</h4>
                        <span>CTO</span>
                    </div>
                </div>
            </div>
            <!-- // Single Team -->
            <!-- Single Team -->
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-team">
                    <div class="team-thumb" style="background-image: url(assets/images/team/terry.jpg)">
                        <div class="team-social">
                            <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-pinterest"></i></a>
                        </div>
                    </div>
                    <div class="team-content">
                        <h4>Terry BACON</h4>
                        <span>Marketing Officer</span>
                    </div>
                </div>
            </div>
            <!-- // Single Team -->
            <!-- Single Team -->
            <div class="col-lg-3 col-md-4 col-sm-6">
                <div class="single-team">
                    <div class="team-thumb" style="background-image: url(assets/images/team/rodolphe.jpg)">
                        <div class="team-social">
                            <a target="_blank" href="#"><i class="fab fa-facebook-f"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-linkedin"></i></a>
                            <a target="_blank" href="#"><i class="fab fa-pinterest"></i></a>
                        </div>
                    </div>
                    <div class="team-content">
                        <h4>Rodolphe CORDIER</h4>
                        <span>Developper</span>
                    </div>
                </div>
            </div>
            <!-- // Single Team -->
        </div>
    </div>
</section>
<!-- ====== // Team Section ====== -->


<!-- ====== Call to Action Area ====== -->
<section class="section-padding call-to-action-aria bg-secondary">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <h2>Need information?</h2>
                <p>Our team is at your disposal to answer your questions as soon as possible.</p>
            </div>
            <div class="col-lg-3">
                <div class="cta-button">
                    <div class="d-table">
                        <div class="d-table-cell">
                            <a href="#" class="button">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ====== // Call to Action Area ====== -->


<!-- ====== Pricing Area ====== -->
<section class="section-padding pb-70 pricing-area">
    <div class="container">
        <!-- Section Title -->
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <div class="section-title text-center">
                    <h2>Pricing Area</h2>
                    <p>Here are our different offers.</p>
                </div>
            </div>
        </div>
        <!-- //Section Title -->
        <div class="row">
            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>current account</h2>

                    </div>
                    <ul>
                        <li>
                            Usually used as a support for complex banking operations,
                            the current account can operate in both debit and credit lines.
                            It can be used for various transactions such as payments and withdrawals in cash,
                            cash and check payments, cash flows by transfers</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->

            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>Young booklet</h2>

                    </div>
                    <ul>
                        <li>Is a regulated savings account reserved for 12/25 year olds. The rate of the passbook is freely fixed by the banks but must
                            be at least equal to 0.75%, which is the current rate of the passbook A</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->
            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>A booklet</h2>

                    </div>
                    <ul>
                        <li>Is a regulated, tax-free, unblocked savings account that has existed since 1818.
                            ELP: The ELP or Housing Savings Plan is a regulated savings
                            vehicle that allows you to obtain a loan at a preferential rate.</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->
        </div>
        <div class="row">
            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>Life insurance</h2>

                    </div>
                    <ul>
                        <li>Life insurance is a financial investment that allows the subscriber to save money for the purpose of transmitting it to a beneficiary
                            when an event related to the insured occurs: his death or his survival.</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->

            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>Consumer credit</h2>

                    </div>
                    <ul>
                        <li>
                            Consumer credit is the category of credit granted to individuals by banks or financial corporations to finance purchases of goods and services,
                            such as large expenditures on capital goods (automobile, household equipment)</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->
            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2> Auto Loan</h2>

                    </div>
                    <ul>
                        <li>

                            Auto Loan is a personal loan used to purchase your car. Its rate varies according to the
                            amount of credit, the duration of the credit but also the type of vehicle you buy (For a new car, the rate may be higher than that obtained for a used car)..</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->
        </div>
        <div class="row">
            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>student loan</h2>

                    </div>
                    <ul>
                        <li>
                            Student loan. To finance their studies, students often use a student loan. As the name suggests the student loan a loan that commits you and must be repaid. ... The amount of
                            this credit varies depending on the banks and your plans for the future</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->

            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>Young booklet</h2>

                    </div>
                    <ul>
                        <li>Is a regulated savings account reserved for 12/25 year olds. The rate of the passbook is freely fixed by the banks but must
                            be at least equal to 0.75%, which is the current rate of the passbook A</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->
            <!-- Single Price Box -->
            <div class="col-lg-4 col-md-6">
                <div class="single-price-box text-center">
                    <div class="price-head">
                        <h2>A booklet</h2>

                    </div>
                    <ul>
                        <li>Is a regulated, tax-free, unblocked savings account that has existed since 1818.
                            ELP: The ELP or Housing Savings Plan is a regulated savings
                            vehicle that allows you to obtain a loan at a preferential rate.</li>
                    </ul>
                    <a href="#" class="button">PURCHASE NOW</a>
                </div>
            </div>
            <!-- // Single Price Box -->
        </div>
    </div>
</section>
<!-- ====== // Pricing Area ====== -->


<!-- ====== Contact Area ====== -->
<section id="contact" class="section-padding contact-section bg-light">
    <div class="container">
        <!-- Section Title -->
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <div class="section-title text-center">
                    <h2>Contact Us</h2>
                    <p>Any additional question? We're here for you.</p>
                </div>
            </div>
        </div>
        <!-- //Section Title -->

        <!-- Contact Form -->
        <div class="row justify-content-center">
            <div class="col-lg-10">
                <!-- Form -->
                <form id="contact-form" action="mail.php" method="post" class="contact-form bg-white">
                    <div class="row">
                        <div class="col-lg-6 form-group">
                            <input type="text" class="form-control" name="name" required placeholder="Name">
                        </div>
                        <div class="col-lg-6 form-group">
                            <input type="email" class="form-control" name="email" required placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="subject" required placeholder="Subject">
                    </div>

                    <div class="form-group">
                        <textarea name="message" id="contact_form" class="form-control" required placeholder="Message"></textarea>
                    </div>
                    <div class="form-btn text-center">
                        <button class="button" type="submit">Send Message</button>
                        <p class="form-message"></p>
                    </div>
                </form>
                <!-- // Form -->
            </div>
        </div>
        <!-- // Contact Form -->
    </div>
</section>
<!-- ====== // Contact Area ====== -->