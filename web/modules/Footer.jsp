<%--
  Created by IntelliJ IDEA.
  User: jamesharrys
  Date: 2019-01-18
  Time: 10:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


        <!-- ====== Footer Area ====== -->
        <footer class="footer-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <div class="copyright-text">
                            <p class="text-white">&copy; 2019 <a href="https://www.globank.com/"> by Globank</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- ====== // Footer Area ====== -->

        <!-- ====== ALL JS ====== -->
        <script src="assets/js/jquery-3.3.1.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
        <script src="assets/js/lightbox.min.js"></script>
        <script src="assets/js/owl.carousel.min.js"></script>
        <script src="assets/js/jquery.mixitup.js"></script>
        <script src="assets/js/wow.min.js"></script>
        <script src="assets/js/typed.js"></script>
        <script src="assets/js/fact.counter.js"></script>
        <script src="assets/js/main.js"></script>

    </body>

</html>