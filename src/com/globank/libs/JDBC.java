package com.globank.libs;

import java.sql.*;
import java.util.List;

import com.globank.beans.BaseModel;

public class JDBC
{
    private static Connection dbConnection = null;
    private static String database  = "mybank";
    private static String user      = "root";
    private static String password  = "123456";
    private static List result;

    public static JDBC instance = null;

    public static JDBC getInstance()
    {
        if (instance == null)
            instance = new JDBC();
        return instance;
    }

    public void connect()
    {
        if( dbConnection == null )
        {
            System.out.println("[DB] Entering Database Connect.");
            try
            {
                Class.forName("com.mysql.jdbc.Driver");
                dbConnection = DriverManager.getConnection("jdbc:mysql://localhost:3306/" + database, user, password);
            }
            catch(Exception e)
            {
                System.out.println(e);
            }
        }
    }

    public void close()
    {
        if( dbConnection != null )
        {
            System.out.println("[DB] Closing database connection.");
            try
            {
                dbConnection.close();

                dbConnection = null;
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }
    }

    public  Integer insert(BaseModel _object)
    {
        Integer _newId = null;

        System.out.println("[DB] Entering INSERT.");

        connect();

        if( dbConnection != null )
        {
            System.out.println("[DB] Got connection. Preparing statement.");

            PreparedStatement _insertQuery = _object.getInsertQuery(dbConnection);

            try
            {
                _insertQuery.executeUpdate();

                System.out.println("[DB] Statement ran. Picking last inserted ID.");

                // ... yeah. Bit strange, but works, so ...
                ResultSet keys = _insertQuery.getGeneratedKeys();
                keys.next();

                _newId = keys.getInt(1);

                System.out.println("[DB] Got last inserted ID. Setting it in object ( maybe for future 'UPDATE' query ? )");

                _object.setId(_newId);
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        close();

        System.out.println("[DB] Exiting INSERT.");

        return _newId;
    }

    public  void update(BaseModel _object)
    {
        System.out.println("[DB] Entering UPDATE.");

        connect();

        if( dbConnection != null )
        {
            System.out.println("[DB] Got connection. Preparing statement.");

            PreparedStatement _updateQuery = _object.getUpdateQuery(dbConnection);

            try
            {
                _updateQuery.execute();

                System.out.println("[DB] Statement ran.");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        close();

        System.out.println("[DB] Exiting UPDATE.");

        return;
    }

    public List select(BaseModel _object)
    {
        System.out.println("[DB] Entering SELECT.");

        connect();

        if( dbConnection != null )
        {
            System.out.println("[DB] Got connection. Preparing statement.");

            PreparedStatement _selectQuery = _object.getSelectQuery(dbConnection);


            try
            {
                _selectQuery.execute();
                ResultSet rs = _selectQuery.executeQuery();
                while( rs.next() ) // Itérations à travers le ResultSet, pour parcourir chaque ligne
                {
                    Integer _colonneInteger = rs.getInt("nomDeLaColonne");
                    result.add(_colonneInteger);
                    String _string = rs.getString("nom de la colonne");
                    result.add(_string);
                }

                System.out.println("[DB] Statement ran.");
            }
            catch (SQLException e)
            {
                e.printStackTrace();
            }
        }

        close();

        System.out.println("[DB] Exiting SELECT.");

        return result;
    }

}
