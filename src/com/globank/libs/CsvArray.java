package com.globank.libs;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class CsvArray
{
    private static String[][] myArray;
    private static int row = 0;
    private static int col = 0;
    private static String InputLine = "";
    private static String xfileLocation;
    private static int productId;

    public static int getProductId() {
        return productId;
    }

    public static void setProductId(int productId) {
        CsvArray.productId = productId;
    }

    public static String getXfileLocation() {
        return xfileLocation;
    }

    public static void setXfileLocation(int productId)
    {
        if (productId == 1)
            CsvArray.xfileLocation = "com/globank/libs/compt.csv";
        else if (productId > 1 && productId < 5)
            CsvArray.xfileLocation = "com/globank/libs/livret.csv";
        else if (productId == 5)
            CsvArray.xfileLocation = "com/globank/libs/assurance.csv";
        else if (productId > 5)
            CsvArray.xfileLocation = "com/globank/libs/credis.csv";
    }

    public static int getRow() {
        return row;
    }

    public static void setRow(int row) {
        CsvArray.row = row;
    }

    public static int getCol() {
        return col;
    }

    public static void setCol(int col) {
        CsvArray.col = col;
    }

    static void getNumOfRowsCols()
    {
        int rows = 0;
        int cols = 0;

        try
        {
            Scanner scanIn = new Scanner(new BufferedReader(new FileReader(getXfileLocation())));
            scanIn.useDelimiter(",");
            while (scanIn.hasNextLine())
            {
                InputLine = scanIn.nextLine();
                String[] inArray = InputLine.split(",");
                rows++;
                cols = inArray.length;
            }
            setRow(rows-1);
            setCol(cols);
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(getRow()+" "+getCol());
    }

    public static String[][] setUpMyCSVArray(int productId)
    {
        setXfileLocation(productId);
        getNumOfRowsCols();
        myArray = new String[getCol()][getRow()];

        double xnum = 0;
        int rowc = 0;

        try
        {
            System.out.println("try");
            Scanner scanIn = new Scanner(new BufferedReader(new FileReader(getXfileLocation())));

            scanIn.nextLine();
            while (scanIn.hasNext())
            {
                InputLine = scanIn.nextLine();
                String[] inArray = InputLine.split(",");
                System.out.println("while");
                for (int x = 0; x < inArray.length; x++)
                {
                    myArray[rowc][x] = inArray[x];
                }
                rowc++;
            }
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        return myArray;

    }

//    static void printMyArray(String file)
//    {
//        getNumOfRowsCols();
//
//        for(int rowc = 0; rowc < getRow(); rowc++)
//        {
//            for(int colc = 0; colc < getCol(); colc++)
//            {
//                System.out.println(myArray[rowc][colc] + " ");
//            }
//            System.out.println();
//        }
//        return;
//    }
}
