package com.globank.servlets;

import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import com.globank.libs.CsvArray;
import com.globank.beans.UserBeans;
import com.globank.libs.JDBC;
import java.util.List;

@WebServlet("/accessibility")
public class AccessibilityServlets extends HttpServlet
{
    private static JDBC jdbc;
    private static CsvArray csv;
    private static String[][][] product;
    private static UserBeans user;

    public static void doGet(ServletRequest request)
    {
        jdbc.getInstance().connect();

        for(int cpt = 1; cpt < 10; cpt++)
        {
            product[cpt] = csv.setUpMyCSVArray(cpt);
        }
        String user_id = request.getParameter("user_id");
        user.setId(Integer.parseInt(user_id));
        List myuser = jdbc.getInstance().select(user);

    }
}
