package com.globank.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Controller")
public class Controller extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        /*response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println("Bienvenue sur Globank!");*/
        /*String message = "Au revoir";
        request.setAttribute("var", message);
        request.setAttribute("time", "night");*/
        String name = request.getParameter("name");
        request.setAttribute("name", name);

        // Locate the wanted JSP file
        // transmit objects request and response to forward the jsp
        this.getServletContext().getRequestDispatcher("index.jsp").forward(request, response);


    }
}
