package com.globank.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "UserLogin")
public class UserLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Locate the wanted JSP file
        // transmit objects request and response to forward the jsp
        this.getServletContext().getRequestDispatcher("/LoginForm.jsp").forward(request, response);

        response.setContentType("text/html;charset=UTF-8");

        try (PrintWriter out = response.getWriter()){
            String _username = request.getParameter("inputusername");
            String _password = request.getParameter("inputpassword");

            try {
                if (_username != null){
                    if (_username.equals("test") && _password.equals("password")){
                        response.sendRedirect("Dashboard.jsp");
                    } else {
                        out.println("Login failed");
                    }
                }
            }

            catch (Exception e){
                out.println("Error: " + e.getMessage());
            }
        }

    }
}
