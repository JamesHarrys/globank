package com.globank.beans;

import java.sql.*;

public class ProductBeans implements BaseModel
{
    private Integer product_id;
    private String product_label;
    private Integer product_type_id;


    private String insertQueryString = "INSERT INTO product ( product_label, product_type_id ) VALUES ( ?, ? );";
    private String updateQueryString = "UPDATE product SET product_label = ?, product_type_id = ? WHERE product_id = ?;";
    private String selectQueryString = "SELECT * FROM product WHERE product_id = ?";

    public Integer getId() {
        return product_id;
    }

    public ProductBeans setId(Integer Id) {
        this.product_id = Id;
        return this;
    }

    public String getProduct_label() {
        return product_label;
    }

    public void setProduct_label(String product_label) {
        this.product_label = product_label;
    }

    public Integer getProduct_type_id() {
        return product_type_id;
    }

    public void setProduct_type_id(Integer product_type_id) {
        this.product_type_id = product_type_id;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getProduct_label());
            statement.setInt (2, this.getProduct_type_id());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getProduct_label());
            statement.setInt (2, this.getProduct_type_id());
            statement.setInt (3, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}

