package com.globank.beans;

import java.sql.*;

public class IncomeBeans implements BaseModel
{
    private Integer income_id;
    private String income_label;
    private Integer income_value;
    private Integer user_id;
    private Integer income_type_id;


    private String insertQueryString = "INSERT INTO income ( income_label, income_value, user_id, income_type_id ) VALUES ( ?, ?, ?, ? );";
    private String updateQueryString = "UPDATE income SET income_label = ?, income_value = ?, user_id = ?, income_type_id = ? WHERE income_id = ?;";
    private String selectQueryString = "SELECT * FROM income WHERE user_id = ?";

    public Integer getId() {
        return income_id;
    }

    public IncomeBeans setId(Integer Id) {
        this.income_id = Id;
        return this;
    }

    public String getIncome_label() {
        return income_label;
    }

    public void setIncome_label(String income_label) {
        this.income_label = income_label;
    }

    public Integer getIncome_value() {
        return income_value;
    }

    public void setIncome_value(Integer income_value) {
        this.income_value = income_value;
    }

    public Integer getUser_id() {
        return user_id;
    }

    public void setUser_id(Integer user_id) {
        this.user_id = user_id;
    }

    public Integer getIncome_type_id() {
        return income_type_id;
    }

    public void setIncome_type_id(Integer income_type_id) {
        this.income_type_id = income_type_id;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getIncome_label());
            statement.setInt (2, this.getIncome_value());
            statement.setInt (3, this.getUser_id());
            statement.setInt (4, this.getIncome_type_id());

        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getIncome_label());
            statement.setInt (2, this.getIncome_value());
            statement.setInt (3, this.getUser_id());
            statement.setInt (4, this.getIncome_type_id());
            statement.setInt (5, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}

