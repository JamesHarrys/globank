package com.globank.beans;

import java.sql.*;

public class FamilyBeans implements BaseModel
{
    private Integer family_id;
    private String family_label;


    private String insertQueryString = "INSERT INTO family ( family_label ) VALUES ( ? );";
    private String updateQueryString = "UPDATE family SET family_label = ? WHERE family_id = ?;";
    private String selectQueryString = "SELECT * FROM family WHERE family_id = ?";

    public Integer getId() {
        return family_id;
    }

    public FamilyBeans setId(Integer Id) {
        this.family_id = Id;
        return this;
    }

    public String getFamily_label() {
        return family_label;
    }

    public void setFamily_label(String family_label) {
        this.family_label = family_label;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getFamily_label());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getFamily_label());
            statement.setInt (2, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}
