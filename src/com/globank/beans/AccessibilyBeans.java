package com.globank.beans;

import java.sql.*;

public class AccessibilyBeans implements BaseModel
{
    private Integer user_id;
    private Integer product_id;
    private Integer accessiblity_subscription;
    private Date accessiblity_date;
    private Integer Accessiblity_subscription_solicitation;

    private String insertQueryString = "INSERT INTO accessibily ( user_id, product_id, accessiblity_subscription, accessiblity_date, Accessiblity_subscription_solicitation) VALUES ( ?, ?, ?, ?, ? );";
    private String updateQueryString = "UPDATE accessibily SET  accessiblity_subscription = ?, accessiblity_date = ?, Accessiblity_subscription_solicitation = ? WHERE user_id = ? AND product_id = ?;";
    private String selectQueryString = "SELECT * FROM accessibily WHERE user_id = ?";

    public Integer getId()
    {
        return user_id;
    }

    public AccessibilyBeans setId(Integer Id)
    {
        this.user_id = Id;
        return this;
    }

    public Integer getProduct_id() {
        return product_id;
    }

    public void setProduct_id(Integer product_id) {
        this.product_id = product_id;
    }

    public Integer getAccessiblity_subscription() {
        return accessiblity_subscription;
    }

    public void setAccessiblity_subscription(Integer accessiblity_subscription) {
        this.accessiblity_subscription = accessiblity_subscription;
    }

    public Date getAccessiblity_date() {
        return accessiblity_date;
    }

    public void setAccessiblity_date(Date accessiblity_date) {
        this.accessiblity_date = accessiblity_date;
    }

    public Integer getAccessiblity_subscription_solicitation() {
        return Accessiblity_subscription_solicitation;
    }

    public void setAccessiblity_subscription_solicitation(Integer accessiblity_subscription_solicitation) {
        Accessiblity_subscription_solicitation = accessiblity_subscription_solicitation;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString)
    {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString()
    {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString)
    {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setInt (1, this.getId());
            statement.setInt (2, this.getProduct_id());
            statement.setInt (3, this.getAccessiblity_subscription());
            statement.setDate (4, this.getAccessiblity_date());
            statement.setInt (5, this.getAccessiblity_subscription_solicitation());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setInt (1, this.getAccessiblity_subscription());
            statement.setDate (2, this.getAccessiblity_date());
            statement.setInt (3, this.getAccessiblity_subscription_solicitation());
            statement.setInt (4, this.getId());
            statement.setInt (5, this.getProduct_id());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

}
