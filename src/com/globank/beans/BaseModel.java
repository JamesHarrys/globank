package com.globank.beans;

import java.sql.Connection;
import java.sql.PreparedStatement;

public interface  BaseModel
{
    // "public abstract" is useless here. Keeping it as a reminder.
    public abstract BaseModel setId(Integer id);
    public abstract PreparedStatement getInsertQuery(Connection dbConnection);
    public abstract PreparedStatement getUpdateQuery(Connection dbConnection);
    public abstract PreparedStatement getSelectQuery(Connection dbConnection);
}
