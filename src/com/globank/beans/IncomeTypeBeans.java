package com.globank.beans;

import java.sql.*;

public class IncomeTypeBeans implements BaseModel
{
    private Integer income_type_id;
    private String income_type_label;


    private String insertQueryString = "INSERT INTO income_type ( income_type_label ) VALUES ( ? );";
    private String updateQueryString = "UPDATE income_type SET income_type_label = ? WHERE income_type_id = ?;";
    private String selectQueryString = "SELECT * FROM income_type WHERE income_type_id = ?";

    public Integer getId() {
        return income_type_id;
    }

    public IncomeTypeBeans setId(Integer Id) {
        this.income_type_id = Id;
        return this;
    }

    public String getIncome_type_label() {
        return income_type_label;
    }

    public void setIncome_type_label(String income_type_label) {
        this.income_type_label = income_type_label;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getIncome_type_label());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getIncome_type_label());
            statement.setInt (2, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}