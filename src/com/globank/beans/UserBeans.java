package com.globank.beans;

import java.sql.*;

public class UserBeans implements BaseModel
{
    private Integer user_id;
    private String user_lastname;
    private String user_firstname;
    private String user_password;
    private Integer user_role;


    private String insertQueryString = "INSERT INTO user ( user_lastname, user_firstname, user_email, user_password, user_role ) VALUES ( ?, ?, ?, ?, ? );";
    private String updateQueryString = "UPDATE user SET user_lastname = ?, user_firstname = ?, user_email = ?, user_password = ?, user_role = ? WHERE user_id = ?;";
    private String selectQueryString = "SELECT * FROM user WHERE user_id = ?";

    public Integer getId() {
        return user_id;
    }

    public UserBeans setId(Integer Id) {
        this.user_id = Id;
        return this;
    }

    public String getUser_lastname() {
        return user_lastname;
    }

    public void setUser_lastname(String user_lastname) {
        this.user_lastname = user_lastname;
    }

    public String getUser_firstname() {
        return user_firstname;
    }

    public void setUser_firstname(String user_firstname) {
        this.user_firstname = user_firstname;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public Integer getUser_role() {
        return user_role;
    }

    public void setUser_role(Integer user_role) {
        this.user_role = user_role;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getUser_lastname());
            statement.setString (2, this.getUser_firstname());
            statement.setString (3, this.getUser_password());
            statement.setInt (4, this.getUser_role());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getUser_lastname());
            statement.setString (2, this.getUser_firstname());
            statement.setString (3, this.getUser_password());
            statement.setInt (4, this.getUser_role());
            statement.setInt (5, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}
