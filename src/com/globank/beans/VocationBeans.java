package com.globank.beans;

import java.sql.*;

public class VocationBeans implements BaseModel
{
    private Integer vocation_id;
    private String vocation_label;


    private String insertQueryString = "INSERT INTO vocation ( vocation_label ) VALUES ( ? );";
    private String updateQueryString = "UPDATE vocation SET vocation_label = ? WHERE vocation_id = ?;";
    private String selectQueryString = "SELECT * FROM vocation WHERE vocation_id = ?";

    public Integer getId() {
        return vocation_id;
    }

    public VocationBeans setId(Integer Id) {
        this.vocation_id = Id;
        return this;
    }

    public String getVocation_label() {
        return vocation_label;
    }

    public void setVocation_label(String family_label) {
        this.vocation_label = family_label;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getVocation_label());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getVocation_label());
            statement.setInt (2, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}

