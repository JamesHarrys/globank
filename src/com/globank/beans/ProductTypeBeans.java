package com.globank.beans;

import java.sql.*;

public class ProductTypeBeans implements BaseModel
{
    private Integer product_type_id;
    private String product_type_label;


    private String insertQueryString = "INSERT INTO product_type ( product_type_label ) VALUES ( ? );";
    private String updateQueryString = "UPDATE product_type SET product_type_label = ? WHERE product_type_id = ?;";
    private String selectQueryString = "SELECT * FROM product_type WHERE product_type_id = ?";

    public Integer getId() {
        return product_type_id;
    }

    public ProductTypeBeans setId(Integer Id) {
        this.product_type_id = Id;
        return this;
    }

    public String getProduct_type_label() {
        return product_type_label;
    }

    public void setProduct_type_label(String product_type_label) {
        this.product_type_label = product_type_label;
    }

    public String getInsertQueryString() {
        return insertQueryString;
    }

    public void setInsertQueryString(String insertQueryString) {
        this.insertQueryString = insertQueryString;
    }

    public String getUpdateQueryString() {
        return updateQueryString;
    }

    public void setUpdateQueryString(String updateQueryString) {
        this.updateQueryString = updateQueryString;
    }

    public String getSelectQueryString() {
        return selectQueryString;
    }

    public void setSelectQueryString(String selectQueryString) {
        this.selectQueryString = selectQueryString;
    }

    public PreparedStatement getInsertQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.insertQueryString, Statement.RETURN_GENERATED_KEYS);
            statement.setString (1, this.getProduct_type_label());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getUpdateQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.updateQueryString);
            statement.setString (1, this.getProduct_type_label());
            statement.setInt (2, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }

    public PreparedStatement getSelectQuery(Connection dbConnection)
    {
        PreparedStatement statement = null;

        try
        {
            statement = dbConnection.prepareStatement(this.selectQueryString);
            statement.setInt (1, this.getId());
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        return statement;
    }
}